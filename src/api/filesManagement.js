import axios from "axios";
import {apiUrl} from "../config";
import {apiFetchRefreshToken} from "./auth";

const config = {
    headers: {
        "Authorization": `Bearer ${localStorage.getItem('accessToken')}`
    }
}

export const apiFetchAllFiles = (accessToken) => {
    console.log(accessToken)
    return axios.get(
        `${apiUrl}docs`,
        {
            headers: {
                "Authorization": `Bearer ${accessToken || localStorage.getItem('accessToken')}`
            }
        }
    ).then(({data}) => data.data)
        .catch(() => {
            apiFetchRefreshToken()
                .then(res => {
                    if (res) {
                        apiFetchAllFiles(res)
                    }
                })
            return false
        })
}

export const apiSaveFile = (file, title, fileId) => {
    const fd = new FormData();
    fd.append('file', file);
    fd.append('title', title);
    fileId && fd.append('fileId', fileId);
    console.log(fd);
    return axios.patch(
        `${apiUrl}docs/save`,
        fd,
        config
    )
}

export const apiDeleteFile = (fileId) => {
    return axios.delete(`${apiUrl}docs/${fileId}`, config)
}

export const parseFileURL = async (url) => {
    return await axios.get(url).then(res => res.data)
}

export const exportDataHandler = (code) => {
    const element = document.createElement("a");
    const file = new Blob([code], {type: 'text/plain'});
    element.href = URL.createObjectURL(file);
    console.log(code, file)
    element.download = "Expert system.txt";
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
}