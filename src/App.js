import React, {useEffect, useRef, useState} from 'react'
import {
    AppWrapper,
    AppWrapperTerminalBody,
} from "./App.styled";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import {authDefaultData, color, splitterDefaultData} from "./config";
import Auth from "./components/Auth/Auth";
import Main from "./components/Main/Main";
import Terminal from "./components/Terminal/Terminal";
import {AuthContext, ExpertSystemsListContext, RenameFileContext, SplitterContext} from "./store/store";
import ConsultingModule from "./components/Modal/ConsultingModule";
import RenameFileModule from "./components/Modal/RenameFileModule";
import { parseFileURL } from './api/filesManagement';

function App() {
    const uploadButtonRef = useRef(null)
    const [authData, setAuthData] = useState(authDefaultData)
    const [renameFileData, setRenameFileData] = useState(null)
    const [expertSystemsList, setExpertSystemsList] = useState([])
    const [splitter, setSplitter] = useState(splitterDefaultData)
    const [code, setCode] = useState('')
    const [parsedCode, setParsedCode] = useState(null)
    const [consultingStatus, setConsultingStatus] = useState(false)
    const [consoleStatus, setConsoleStatus] = useState(false)
    const [choosenFile, chooseFile] = useState('')
    const [selectedFile, selectFile] = useState('')
    const [mimeType, setMimeType] = useState('')
    const [fileName, setFileName] = useState('')
    const [file, setFile] = useState('')
    const [logCounter, setLogCounter] = useState(0)

    const consultingTitle = expertSystemsList?.filter(es => es.active)[0]?.title || 'Faculty finder'; 

    const consoleStatusHandler = () => {
        setLogCounter(0)
        setConsoleStatus(!consoleStatus)
        if (!consoleStatus) {
            setLogCounter(0)
        }
    }

    const eslParser = (str, opts) => {
        const options = opts || {};
        const {logs} = options;

        const kbObject = {
            data: {},
            rules: []
        };

        const dataObj = str
            .split(';')
            .map(row => row.replace(/(\r\n|\n|\r)/gm, '').trim())
            .filter(str => str)
            .map(row => {
                if (row.substr(0, 5) === 'allow') {
                    const rowData = row.split('=');
                    const parsedKey = rowData[0]
                        .replace('allow', '')
                        .replace('(', '')
                        .replace(')', '')
                        .replace(/\"/gm, '')
                        .trim();
                    const parsedValues = rowData[1].split(',').map(v => v.trim());
                    kbObject.data[parsedKey] = {allows: parsedValues};
                } else if (row.substr(0, 8) === 'question') {
                    const rowData = row.split('=');
                    const parsedKey = rowData[0]
                        .replace('question', '')
                        .replace('(', '')
                        .replace(')', '')
                        .replace(/\"/gm, '')
                        .trim();
                    const parsedValues = rowData[1].replace(/\"/gm, '').trim();

                    kbObject.data[parsedKey] = {
                        questions: parsedValues,
                        allows: kbObject.data[parsedKey].allows,
                    };
                } else if (row.substr(0, 4) === 'rule') {
                    const rowData = row.split(':')[1].split('then');
                    const ruleObj = {
                        conditions: {},
                        action: {}
                    };

                    // if( rowData[0].includes('&') ){
                    rowData[0].split('&').forEach(cond => {
                        const condData = cond.split('=');
                        ruleObj.conditions[condData[0].trim()] = condData[1].trim();
                    });
                    // }else{
                    //     const condData = rowData[0].split('=');
                    //     ruleObj[ condData[0] ] = condData[1];
                    // }
                    rowData[1].split('&').forEach(cond => {
                        const condData = cond.split('=');
                        ruleObj.action[condData[0].trim()] = condData[1].trim();
                    });

                    kbObject.rules.push(ruleObj);
                } else {
                    setLog('error', 'error', 'Line must start on allow | question | rule');
                }
                return row;
            });

        let i = 1;

        if (logs) {
            dataObj.forEach(row => {
                console.log(`${i++} | ` + JSON.stringify(row));
            });
        }

        return kbObject;
    };

    function readTextFile(file) {
        let rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, false);
        rawFile.onreadystatechange = function () {
            if (rawFile.readyState === 4) {
                if (rawFile.status === 200 || rawFile.status == 0) {
                    let allText = rawFile.responseText;
                    const cText = eslParser(allText)
                    setParsedCode(cText)
                    setCode(JSON.stringify(cText, undefined, 2));
                }
            }
        }
        rawFile.send(null);
    }

    const uploadFileHandler = event => {
        let file = event.target.files[0];
        const reader = new FileReader();
        let url = file && reader.readAsDataURL(file);
        const id = Math.random(),
            addedFiles = []

        if (event.target.files.length !== 0) {
            reader.onprogress = function (event) {
                const loaded = event.loaded,
                    total = event.total
                const data = {
                    progress: Math.floor((loaded / total) * 100),
                    id: id
                }
                if (total < 10000000) {
                    if (addedFiles.filter(e => e.id === id).length === 0) {
                        addedFiles.push(
                            ...addedFiles,
                            data
                        )
                    }
                }
            };

            reader.onloadend = function (e) {
                const total = e.total
                if (total < 10000000) {
                    chooseFile([reader.result])
                    selectFile(file)
                    readTextFile([reader.result])
                    setMimeType(file.type)
                    setFileName(file.name)
                    setFile(reader.result.slice(reader.result.indexOf(',') + 1))
                    setExpertSystemsList([
                            ...expertSystemsList.map(e => ({...e, active: false})),
                            {
                                new: true,
                                active: true,
                                id: new Date().getTime(),
                                title: file.name || `Expert system ${expertSystemsList.length + 1}`,
                                file: [reader.result]
                            }
                        ]
                    )
                }
            }.bind(this);
        }
    }

    const controlUploadButtonHandler = () => uploadButtonRef?.current?.click()

    const startConsultationHandler = () => setConsultingStatus(!consultingStatus)

    const setLog = (type, key, value) => {
        const allowedTypes = {
            'warning': {
                backgroundColor: color.keyWord2,
                textColor: '#000'
            },
            'error': {
                backgroundColor: color.keyWord3,
                textColor: '#000'
            },
        }
        setLogCounter(prevState => prevState + 1)
        if (!Object.keys(allowedTypes).includes(type)) {
            throw new Error('Invalid log type')
        }
        return (
            <AppWrapperTerminalBody
                textColor={allowedTypes[type].textColor}
                backgroundColor={allowedTypes[type].backgroundColor}
            >
                {key}: {value}
            </AppWrapperTerminalBody>
        )
    }

    console.log(expertSystemsList);

    useEffect(() => {
        if (expertSystemsList.filter(es => es.active)[0]?.url) {
            parseFileURL(
                expertSystemsList.filter(es => es.active)[0]?.url
                .replace('https', 'http')
            ).then(res => {
                setParsedCode(res)
                setCode(JSON.stringify(res, undefined, 2))
            })
        } else if (expertSystemsList.filter(es => es.active)[0]?.file) {
            readTextFile(expertSystemsList.filter(es => es.active)[0]?.file)
        } else {
            setCode('')
        }
    }, [expertSystemsList])

    return (
        <AuthContext.Provider value={[authData, setAuthData]}>
            <RenameFileContext.Provider value={[renameFileData, setRenameFileData]}>
                <ExpertSystemsListContext.Provider value={[expertSystemsList, setExpertSystemsList]}>
                    <SplitterContext.Provider value={[splitter, setSplitter]}>
                        <AppWrapper>
                            {
                                !authData?.authorised
                                    ? <Auth/>
                                    : <>
                                        {
                                            consultingStatus &&
                                            <Modal
                                                title={`Consulting - ${consultingTitle}`}
                                                body={
                                                    <ConsultingModule
                                                        parsedCode={parsedCode}
                                                        setConsultingStatus={setConsultingStatus}
                                                    />
                                                }
                                                setModalStatus={() => setConsultingStatus(false)}
                                            />
                                        }
                                        {
                                            renameFileData?.status &&
                                            <Modal
                                                title={'Rename file'}
                                                body={<RenameFileModule/>}
                                                setModalStatus={() => setRenameFileData(prevState => [{
                                                    ...prevState,
                                                    status: false
                                                }])}
                                            />
                                        }
                                        <Header
                                            code={code}
                                            uploadButtonRef={uploadButtonRef}
                                            uploadFileHandler={uploadFileHandler}
                                            startConsultationHandler={startConsultationHandler}
                                            controlUploadButtonHandler={controlUploadButtonHandler}
                                        />
                                        <Main
                                            code={code}
                                            setCode={setCode}
                                            consoleStatus={consoleStatus}
                                        />
                                        <Terminal
                                            consoleStatus={consoleStatus}
                                            consoleStatusHandler={consoleStatusHandler}
                                            logCounter={logCounter}
                                            setLog={setLog}
                                        />
                                    </>
                            }
                        </AppWrapper>
                    </SplitterContext.Provider>
                </ExpertSystemsListContext.Provider>
            </RenameFileContext.Provider>
        </AuthContext.Provider>
    );
}

export default App;
