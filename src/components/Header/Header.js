import React, {useContext, useState, useEffect} from 'react'
import {
    HeaderWrapper,
    HeaderWrapperAuthButtons,
    HeaderWrapperAuthButtonsSignOut,
    HeaderWrapperAuthButtonsUsername,
    HeaderWrapperAuthButtonsWrapper,
    HeaderWrapperAuthButtonsWrapperList,
    HeaderWrapperAuthButtonsWrapperListItem,
    HeaderWrapperButtons,
    HeaderWrapperLogo,
    HeaderWrapperStartConsultation,
    HeaderWrapperUpload,
    HeaderWrapperUploadButton
} from "./Header.styled";
import Logo from '../../assets/logo.png'
import {AuthContext, ExpertSystemsListContext} from "../../store/store";
import { exportDataHandler } from '../../api/filesManagement';

function Header(props) {
    const {
        code,
        uploadButtonRef,
        uploadFileHandler,
        startConsultationHandler,
        controlUploadButtonHandler,
    } = props
    const [authData, setAuthData] = useContext(AuthContext)
    const {user} = authData
    const [expertSystemsList, setExpertSystemsList] = useContext(ExpertSystemsListContext)
    const [listStatus, setListStatus] = useState(false)

    const signOutHandler = () => {
        setAuthData({
            ...authData,
            authorised: false,
            signInStatus: true
        })
        localStorage.clear()
    }

    const listHandler = () => setListStatus(!listStatus)

    const openFileHandler = (id) => {
        setExpertSystemsList(expertSystemsList.map((es) => es.id === id ? {
            ...es,
            active: true,
            closed: false
        } : {
            ...es,
            active: false
        }))
    }

    return (
        <HeaderWrapper>
            <HeaderWrapperLogo
                src={Logo}
                alt={'Logo'}
            />
            <HeaderWrapperButtons>
                <HeaderWrapperUpload
                    type={'file'}
                    ref={uploadButtonRef}
                    onChange={uploadFileHandler}
                />
                <HeaderWrapperUploadButton
                    onClick={controlUploadButtonHandler}
                >
                    {'Upload'}
                </HeaderWrapperUploadButton>
                <HeaderWrapperStartConsultation
                    onClick={() => exportDataHandler(code)}
                >
                    {'Export'}
                </HeaderWrapperStartConsultation>
                <HeaderWrapperStartConsultation
                    onClick={startConsultationHandler}
                >
                    {'Start consulting'}
                </HeaderWrapperStartConsultation>
            </HeaderWrapperButtons>
            <HeaderWrapperAuthButtons>
                <HeaderWrapperAuthButtonsWrapper>
                    <HeaderWrapperAuthButtonsUsername
                        isEmptyList={!expertSystemsList.length}
                        status={expertSystemsList.length && listStatus}
                        onClick={listHandler}
                    >
                        {user?.name || 'Guest'}
                    </HeaderWrapperAuthButtonsUsername>
                    <HeaderWrapperAuthButtonsWrapperList
                        status={expertSystemsList.length && listStatus}
                    >
                        {
                            expertSystemsList?.map(({title, id}, i) =>
                                <HeaderWrapperAuthButtonsWrapperListItem
                                    key={id}
                                    onClick={() => openFileHandler(id)}
                                >
                                    {title || `Expert system ${i + 1}`}
                                </HeaderWrapperAuthButtonsWrapperListItem>
                            )
                        }
                    </HeaderWrapperAuthButtonsWrapperList>
                </HeaderWrapperAuthButtonsWrapper>
                <HeaderWrapperAuthButtonsSignOut
                    onClick={signOutHandler}
                >
                    {'Sign Out'}
                </HeaderWrapperAuthButtonsSignOut>
            </HeaderWrapperAuthButtons>
        </HeaderWrapper>
    )
}

export default Header