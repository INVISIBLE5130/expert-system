import styled from 'styled-components'
import {color} from "../../config";
import {AppWrapperMainThirdAreaUploadButton} from "../../App.styled";

export const HeaderWrapper = styled.div`
  gap: 30px;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  padding: 2vh 5vw 2vh calc(5vw - 25px);
  align-items: center;
  background-color: ${color.consoleHeader};

  @media (max-width: 768px) {
    padding: 2vh 5vw;
    flex-direction: column;
  }
`

export const HeaderWrapperLogo = styled.img`
  width: 100px;
`

export const HeaderWrapperButtons = styled.div`
  display: flex;
`

export const HeaderWrapperUpload = styled.input`
  display: none;
`

export const HeaderWrapperUploadButton = styled.button`
  border: none;
  outline: none;
  cursor: pointer;
  font-weight: bold;
  width: max-content;
  padding: 10px 15px;
  border-radius: 7px;
  height: max-content;
  color: ${color.buttonText};
  background: ${color.buttonBackground};
`

export const HeaderWrapperStartConsultation = styled(HeaderWrapperUploadButton)`
  margin-left: 10px;
`

export const HeaderWrapperAuthButtons = styled.div`
  gap: 3rem;
  display: flex;
  margin: 0 0 0 auto;
  align-items: center;

  @media (max-width: 768px) {
    gap: 1rem;
    margin: 0;
    flex-direction: column;
  }
`

export const HeaderWrapperAuthButtonsWrapper = styled.div`
  position: relative;
`

export const HeaderWrapperAuthButtonsWrapperList = styled.div`
  top: 5vh;
  z-index: 10;
  display: flex;
  overflow: hidden;
  transition: .25s;
  position: absolute;
  border-radius: 1rem;
  flex-direction: column;
  background-color: white;
  opacity: ${p => p.status ? 1 : 0};
  visibility: ${p => p.status ? 'visible' : 'hidden'};
  box-shadow: 0 0 10px 5px rgba(0, 0, 0, 0.25);
`

export const HeaderWrapperAuthButtonsWrapperListItem = styled.p`
  cursor: pointer;
  padding: 1vh 1vw;
  width: max-content;
  
  &:hover {
    background-color: ${color.buttonBackground};
  }
`

export const HeaderWrapperAuthButtonsUsername = styled.button`
  border: none;
  outline: none;
  cursor: ${p => p.isEmptyList ? 'initial' : 'pointer'};
  background-color: unset;
  
  &:after {
    top: 0;
    bottom: 0;
    right: -1rem;
    content: '>';
    margin: auto 0;
    display: ${p => p.isEmptyList ? 'none' : 'block'};
    position: absolute;
    transform: rotate(${p => p.status ? '270deg' : '90deg'});
  }
`

export const HeaderWrapperAuthButtonsSignOut = styled(HeaderWrapperUploadButton)`
`