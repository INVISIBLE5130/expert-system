import React, {useContext} from "react";
import {AppWrapperMain, AppWrapperMainFirstArea, AppWrapperSplitter} from "../../App.styled";
import Editor from "react-simple-code-editor";
import {highlight, languages} from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';
import 'prismjs/themes/prism.css';
import Tabs from "../Tabs/Tabs";
import CreateFile from "./CreateFile/CreateFile";
import {ExpertSystemsListContext} from "../../store/store";

function Main({code, setCode}) {
    const [expertSystemsList, setExpertSystemsList] = useContext(ExpertSystemsListContext)
    const createFirstFileStatus = expertSystemsList?.length === 0

    const fileChangesHandler = (text) => {
        console.log(text);
        const file = new Blob([text], {type: 'text/plain'});
        console.log(file);
        setCode(text)
        setExpertSystemsList(expertSystemsList?.map(es => es?.active ? {
            ...es,
            file,
            code: text
        } : es))
    }

    return (
        <AppWrapperSplitter column={true}>
            {
                createFirstFileStatus
                ? <CreateFile/>
                : <AppWrapperMain>
                    <Tabs list={expertSystemsList}/>
                    <AppWrapperMainFirstArea column={true}>
                        <Editor
                            value={expertSystemsList?.filter(es => es?.active)[0]?.code || code}
                            onValueChange={(code) => fileChangesHandler(code)}
                            highlight={(code) => highlight(code, languages.js)}
                            padding={10}
                            style={{
                                fontFamily: '"Fira code", "Fira Mono", monospace',
                                fontSize: 12,
                                width: '104%',
                                height: '100%',
                                overflowY: 'auto',
                                boxSizing: 'content-box'
                            }}
                        />
                    </AppWrapperMainFirstArea>
                </AppWrapperMain>
            }
        </AppWrapperSplitter>
    )
}

export default Main