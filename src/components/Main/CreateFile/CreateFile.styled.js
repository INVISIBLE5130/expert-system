import styled from 'styled-components';
import { color } from '../../../config';

export const CreateFileStyled = styled.div`
  margin: auto;
  border: none;
  outline: none;
  cursor: pointer;
  font-weight: bold;
  width: max-content;
  padding: 10px 15px;
  border-radius: 7px;
  height: max-content;
  color: ${color.buttonText};
  background: ${color.buttonBackground};
`