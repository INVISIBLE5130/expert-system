import React, {useContext} from 'react';
import { ExpertSystemsListContext } from '../../../store/store';
import { CreateFileStyled } from './CreateFile.styled';

function CreateFile() {
    const [expertSystemsList, setExpertSystemsList] = useContext(ExpertSystemsListContext)

    const createFileHandler = () => {
        setExpertSystemsList([
                ...expertSystemsList.map(es => ({...es, active: false})),
                {
                    new: true,
                    active: true,
                    id: new Date().getTime(),
                    title: `Expert system ${expertSystemsList.length + 1}`,
                }
            ]
        )
    }

    return (
        <CreateFileStyled onClick={createFileHandler}>
            Create your first expert system
        </CreateFileStyled>
    )
}

export default CreateFile