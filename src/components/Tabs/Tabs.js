import React, {useContext} from "react";
import {TabsScrollWrapper, TabsWrapper, TabsWrapperTabCreate} from "./Tabs.styled";
import Tab from "./Tab";
import CreateIcon from '../../assets/close.svg'
import { ExpertSystemsListContext } from "../../store/store";

const Tabs = ({list, listIndex}) => {
    const [expertSystemsList, setExpertSystemsList] = useContext(ExpertSystemsListContext)
    const createFileStatus = list.length < 10

    const createTabHandler = () => {
        setExpertSystemsList([
                ...expertSystemsList.map(es => ({...es, active: false})),
                {
                    new: true,
                    active: true,
                    id: new Date().getTime(),
                    title: `Expert system ${expertSystemsList.length + 1}`,
                }
            ]
        )
    }

    return (
        <TabsScrollWrapper>
            <TabsWrapper>
                {list?.map((es, esIndex) => !es?.closed && <Tab list={list} es={es} listIndex={esIndex}/>)}
                {
                    createFileStatus && 
                    <TabsWrapperTabCreate
                        src={CreateIcon}
                        alt={'Create'}
                        onClick={createTabHandler}
                    />
                }   
            </TabsWrapper>
        </TabsScrollWrapper>
    )
}

export default Tabs