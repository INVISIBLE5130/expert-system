import styled from "styled-components";
import {color} from "../../../config";

export const TabMenuWrapper = styled.div`
  top: ${p => p.top}px;
  left: ${p => p.left}px;
  z-index: 1;
  width: ${p => p.width ? `${p.width}px` : '100%'};
  display: flex;
  position: absolute;
  flex-direction: column;
`

export const TabMenuWrapperItem = styled.div`
  gap: .5rem;
  display: flex;
  cursor: pointer;
  padding: .5rem 1rem;
  align-items: center;
  border-left: 3px solid ${color.buttonBackground};
  background-color: ${color.buttonBackground};

  &:hover {
    border-left: 3px solid #ffffff;
  }
`

export const TabMenuWrapperItemIcon = styled.img`
  width: 1rem;
  height: 1rem;
  filter: brightness(10) !important;
`

export const TabMenuWrapperItemTitle = styled.p`
    color: white;
`