import React, {useContext, useEffect, useRef} from "react";
import {TabMenuWrapper, TabMenuWrapperItem, TabMenuWrapperItemIcon, TabMenuWrapperItemTitle} from "./TabMenu.styled";
// import SplitRightIcon from '../../../assets/splitRight.png'
// import SplitDownIcon from '../../../assets/splitDown.png'
import RenameIcon from '../../../assets/edit.png'
import SaveIcon from '../../../assets/save.png'
import DeleteIcon from '../../../assets/delete.png'
import {ExpertSystemsListContext, RenameFileContext, SplitterContext} from "../../../store/store";
import { apiDeleteFile, apiSaveFile } from "../../../api/filesManagement";

function TabMenu({top, left, width, es, listIndex, tabMenuHandler}) {
    const vw = window.innerWidth / 100
    const menuRef = useRef(null)
    const [splitter, setSplitter] = useContext(SplitterContext)
    const [expertSystemsList, setExpertSystemsList] = useContext(ExpertSystemsListContext)
    const [renameFileData, setRenameFileData] = useContext(RenameFileContext)

    useEffect(() => {
        function handleClickOutside(e) {
            if (menuRef?.current && !menuRef?.current.contains(e.target)) {
                tabMenuHandler()
            }
        }

        document.addEventListener('click', handleClickOutside)

        return () => {
            document.removeEventListener('click', handleClickOutside)
        }
    }, [menuRef])

    const splitterHandler = async (column) => {
        console.log({
            column: column,
            spaces: [
                splitter?.spaces[0],
                [es]
            ]
        })
        await setSplitter({
            column: column,
            spaces: [
                splitter?.spaces[0],
                [es]
            ]
        })
        tabMenuHandler()
    }

    const renameWindowHandler = () => {
        setRenameFileData(prevState => ({
            ...prevState,
            listIndex: listIndex,
            fileId: es?.id,
            value: es?.title,
            status: true
        }))
    }

    console.log(es);

    const saveFileHandler = () => {
        console.log(es);
        if (es?.code) {
            apiSaveFile(es?.file, es?.title, !es?.new && es?.id)
        }
    }

    const deleteFileHandler = () => {
        console.log(expertSystemsList.filter(({id}) => id !== es?.id));
        setExpertSystemsList(expertSystemsList.filter(({id}) => id !== es?.id))
        if (!es?.new) {
            apiDeleteFile(es?.id)
        }
    }

    const menu = [
        // {
        //     title: 'Split right',
        //     icon: SplitRightIcon,
        //     action: () => splitterHandler(false)
        // },
        // {
        //     title: 'Split down',
        //     icon: SplitDownIcon,
        //     action: () => splitterHandler(true)
        // },
        {
            title: 'Rename',
            icon: RenameIcon,
            action: () => renameWindowHandler()
        },
        {
            title: 'Delete',
            icon: DeleteIcon,
            action: () => deleteFileHandler()
        },
        {
            title: 'Save',
            icon: SaveIcon,
            action: () => saveFileHandler()
        },
    ]

    return (
        <TabMenuWrapper
            ref={menuRef}
            top={top}
            left={left - (vw * 5)}
            width={width}
        >
            {
                menu.map(({title, icon, action}) => (
                    <TabMenuWrapperItem
                        onClick={action}
                    >
                        <TabMenuWrapperItemIcon src={icon} alt={title}/>
                        <TabMenuWrapperItemTitle>
                            {title}
                        </TabMenuWrapperItemTitle>
                    </TabMenuWrapperItem>
                ))
            }
        </TabMenuWrapper>
    )
}

export default TabMenu