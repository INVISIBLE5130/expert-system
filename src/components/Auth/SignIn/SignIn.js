import React, {useContext, useState} from "react";
import {
    SignInWrapper, SignInWrapperInput, SignInWrapperSubmit,
} from './SignIn.styled'
import {AuthContext, ExpertSystemsListContext} from "../../../store/store";
import axios from "axios";
import {apiUrl, splitterDefaultData} from "../../../config";
import {apiFetchLogin} from "../../../api/auth";
import {apiFetchAllFiles} from "../../../api/filesManagement";

function SignIn() {
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    const [authData, setAuthData] = useContext(AuthContext)
    const [splitter, setSplitter] = useState(splitterDefaultData)
    const [expertSystemsList, setExpertSystemsList] = useContext(ExpertSystemsListContext)

    const signInHandler = () => {
        apiFetchLogin(login, password)
            .then(user => {
                if (user) {
                    setAuthData({
                        ...authData,
                        user,
                        authorised: true
                    })
                    apiFetchAllFiles()
                        .then(list => {
                            if (list) {
                                setSplitter({
                                    ...splitter,
                                    spaces: list
                                })
                                setExpertSystemsList(
                                    list.map((es, i) => i === 0 ? {
                                        ...es,
                                        active: true
                                    } : es)
                                )
                            } else {
                                setAuthData({
                                    ...authData,
                                    authorised: false,
                                    signInStatus: true
                                })
                            }
                        })
                } else {
                    alert('Invalid credentials.')
                }
            })
    }

    return (
        <SignInWrapper>
            <SignInWrapperInput
                type={'email'}
                placeholder={'E-mail'}
                value={login}
                onChange={e => setLogin(e.target.value)}
            />
            <SignInWrapperInput
                type={'password'}
                placeholder={'Password'}
                value={password}
                onChange={e => setPassword(e.target.value)}
            />
            <SignInWrapperSubmit onClick={signInHandler}>
                Sign In
            </SignInWrapperSubmit>
        </SignInWrapper>
    )
}

export default SignIn