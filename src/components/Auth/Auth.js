import React, {useContext} from "react";
import {
    AuthWrapper,
    AuthWrapperTitle, AuthWrapperWindow, AuthWrapperWindowSwitch,
} from './Auth.styled'
import {AuthContext} from "../../store/store";
import SignIn from "./SignIn/SignIn";
import SignUp from "./SignUp/SignUp";

function Auth() {
    const [authData, setAuthData] = useContext(AuthContext)
    const {signInStatus} = authData

    const signInStatusHandler = () => {
        setAuthData({
            ...authData,
            signInStatus: !signInStatus
        })
    }

    return (
        <AuthWrapper>
            <AuthWrapperTitle>
                Expert System
            </AuthWrapperTitle>
            <AuthWrapperWindow>
                {
                    signInStatus
                        ? <SignIn/>
                        : <SignUp/>
                }
                <AuthWrapperWindowSwitch>
                    {
                        signInStatus
                            ? <>Don't have an account? <b onClick={signInStatusHandler}>Sign Up</b></>
                            : <>Already have an account? <b onClick={signInStatusHandler}>Sign In</b></>
                    }
                </AuthWrapperWindowSwitch>
            </AuthWrapperWindow>
        </AuthWrapper>
    )
}

export default Auth