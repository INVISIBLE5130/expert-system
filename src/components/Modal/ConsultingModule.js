import React, {useEffect, useState} from "react";
import {
    ModalWrapperWindowBodyAnswerButton,
    ModalWrapperWindowBodyAnswerButtons,
    ModalWrapperWindowBodyAnswerButtonsItem,
    ModalWrapperWindowBodyAnswerInput,
    ModalWrapperWindowBodyQuestion,
    ModalWrapperWindowResultTitle
} from "./Modal.styled";
import {remoteData as rData} from "../../SES_KB";

const ConsultingModule = ({parsedCode, setConsultingStatus}) => {
    //Remote data
    const [remoteData, setRemoteData] = useState([])
    //Search key
    const [searchKey, setSearchKey] = useState('')
    //Global state
    const [data, setData] = useState({
        searchKey: '',
        localKeys: {}
    })
    const [answerStatus, setAnswerStatus] = useState(false)

    useEffect(() => {
        //If we get an answer we setStatus of answer to true and it causes to start survey again survey
        const answer = JSON.stringify(parsedCode.rules.filter(e => JSON.stringify(e.conditions) === JSON.stringify(data.localKeys))[0]?.action[data.searchKey])
        if (answer)
            setAnswerStatus(true)
    }, [parsedCode, data])

    useEffect(() => {
        if (parsedCode !== {}) {
            //Getting parsed code from parser by props and setting it to state
            setRemoteData(Object.entries(parsedCode?.data))
        }
    }, [parsedCode])

    const searchKeyHandler = e => {
        const value = e.target.value
        //Getting search key value and setting it to state
        setSearchKey(value)
    }

    const setSearchKeyHandler = () => {
        //Setting search key value to global state, where we store answers and search key
        setData({
            searchKey: searchKey,
            localKeys: {}
        })
    }

    const closeModalHandler = () => {
        //Closing modal(survey) and setting all states to default
        setConsultingStatus(false)
        setSearchKey('')
        setData({
            searchKey: '',
            localKeys: {}
        })
        setAnswerStatus(false)
    }

    const setQuestion = (question, buttons, q) => {
        //Here we are setting new questions
        return (
            <>
                <ModalWrapperWindowBodyQuestion>
                    {question}
                </ModalWrapperWindowBodyQuestion>
                <ModalWrapperWindowBodyAnswerButtons>
                    {
                        buttons?.map(button => {
                            return (
                                <ModalWrapperWindowBodyAnswerButtonsItem
                                    onClick={() => answerIndexHandler({
                                        question: q,
                                        answer: button
                                    })}
                                >
                                    {button}
                                </ModalWrapperWindowBodyAnswerButtonsItem>
                            )
                        })
                    }
                </ModalWrapperWindowBodyAnswerButtons>
            </>
        )
    }

    const tryAgainHandler = () => {
        //In this part, when pass the survey until the end we could press "Tru again" and it will set our states to default values
        setSearchKey('')
        setData({
            searchKey: '',
            localKeys: {}
        })
        setRemoteData(Object.entries(rData?.data))
        setAnswerStatus(false)
    }

    const showResult = result => {
        //Here we are showing result of our survey
        return (
            <>
                <ModalWrapperWindowResultTitle>
                    {'Expertise result:'}
                </ModalWrapperWindowResultTitle>
                <ModalWrapperWindowResultTitle>
                    {result ? `${data.searchKey} - ${result}` : 'No data'}
                </ModalWrapperWindowResultTitle>
                <ModalWrapperWindowBodyAnswerButtons>
                    <ModalWrapperWindowBodyAnswerButtonsItem
                        onClick={tryAgainHandler}
                    >
                        {'Try again'}
                    </ModalWrapperWindowBodyAnswerButtonsItem>
                    <ModalWrapperWindowBodyAnswerButtonsItem
                        onClick={closeModalHandler}
                    >
                        {'Exit'}
                    </ModalWrapperWindowBodyAnswerButtonsItem>
                </ModalWrapperWindowBodyAnswerButtons>
            </>
        )
    }

    const answerIndexHandler = (d) => {
        //Here we are writing our answers to global state and filtering remote data by excluding question on which we get an answer
        setRemoteData(remoteData.filter(e => e[0] !== d.question))
        setData({
            searchKey: searchKey,
            localKeys: {
                ...data.localKeys,
                [d.question]: d.answer
            }
        })
    }

    return data.searchKey === ''
        ? <>
            <ModalWrapperWindowBodyQuestion>
                {'What is your search key?'}
            </ModalWrapperWindowBodyQuestion>
            <ModalWrapperWindowBodyAnswerInput
                value={searchKey}
                onChange={searchKeyHandler}
            />
            <ModalWrapperWindowBodyAnswerButton
                onClick={setSearchKeyHandler}
            >
                {'Send'}
            </ModalWrapperWindowBodyAnswerButton>
        </>
        : (remoteData.length !== 0 && !answerStatus)
            ? setQuestion(remoteData[0][1]?.question, remoteData[0][1]?.allows, remoteData[0][0])
            : showResult(JSON.stringify(parsedCode.rules.filter(e => JSON.stringify(e.conditions) === JSON.stringify(data.localKeys))[0]?.action[data.searchKey]))
}

export default ConsultingModule