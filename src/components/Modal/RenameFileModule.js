import React, {useContext, useState} from "react";
import {
    ModalWrapperWindowBodyAnswerButtons,
    ModalWrapperWindowBodyAnswerButtonsItem,
    ModalWrapperWindowBodyAnswerInput
} from "./Modal.styled";
import {RenameFileContext, SplitterContext} from "../../store/store";

const RenameFileModule = () => {
    const [splitter, setSplitter] = useContext(SplitterContext)
    const [renameFileData, setRenameFileData] = useContext(RenameFileContext)
    const [newName, setNewName] = useState(renameFileData?.value || '')

    const closeModalHandler = () => setRenameFileData(prevState => ({...prevState, status: false}))

    const setFileNameHandler = () => {
        setSplitter(prevState => ({
            ...prevState,
            spaces: prevState.spaces.map((space, spaceIndex) =>
                spaceIndex === renameFileData?.listIndex
                    ? space.map(list =>
                        list?.id === renameFileData?.fileId
                            ? {
                                ...list,
                                title: newName
                            }
                            : list
                    )
                    : space
            )
        }), closeModalHandler())
    }

    return <>
        <ModalWrapperWindowBodyAnswerInput
            value={newName}
            onChange={(e) => setNewName(e.target.value)}
        />
        <ModalWrapperWindowBodyAnswerButtons>
            <ModalWrapperWindowBodyAnswerButtonsItem
                onClick={setFileNameHandler}
            >
                {'Rename'}
            </ModalWrapperWindowBodyAnswerButtonsItem>
            <ModalWrapperWindowBodyAnswerButtonsItem
                onClick={closeModalHandler}
            >
                {'Cancel'}
            </ModalWrapperWindowBodyAnswerButtonsItem>
        </ModalWrapperWindowBodyAnswerButtons>
    </>
}

export default RenameFileModule