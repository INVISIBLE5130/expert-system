export const apiUrl = 'http://54.235.10.99:3000/api/'

export const color = {
    code: '#1E1F26',
    codeBackground: '#ffffff',
    border: '#000000',
    consoleHeader: '#ffffff',
    consoleFont: '#000000',
    keyWord1: '#90b061',
    keyWord2: '#ffb169',
    keyWord3: '#ff7f85',
    defaultSymbols: '#9da39d',
    buttonBackground: '#FF7E48',
    buttonBackgroundHover: '#000000',
    buttonText: '#ffffff',
    tabBorder: '#FF7E48',
    tabBackgroundHover: '#FF7E48',
}

export const authDefaultData = {
    name: 'Guest',
    authorised: false,
    signInStatus: true,
}

export const defaultList = [
    {
        id: 1,
        title: 'Expert system 1',
        active: false,
    },
    {
        id: 2,
        title: 'Expert system 2',
        active: false,
    },
    {
        id: 3,
        title: 'Expert system 3',
        active: false,
    },
    {
        id: 4,
        title: 'Expert system 4',
        active: false,
    },
    {
        id: 5,
        title: 'Expert system 5',
        active: false,
    },
    {
        id: 6,
        title: 'Expert system 6',
        active: false,
    },
    {
        id: 7,
        title: 'Expert system 7',
        active: false,
    },
    {
        id: 8,
        title: 'Expert system 8',
        active: false,
    },
    {
        id: 9,
        title: 'Expert system 9',
        active: false,
    },
    {
        id: 10,
        title: 'Expert system 10',
        active: false,
    },
]

export const splitterDefaultData = {
    column: true,
    spaces: []
    // spaces: [defaultList]
}